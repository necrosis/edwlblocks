# Uncomment to build with systemd libs
DBUS_LIBS = libsystemd
DBUS_FLAGS = -DWITH_SYSTEMD

# Uncomment to build with elogind libs
# DBUS_LIBS = libelogind
# DBUS_FLAGS = -DWITH_ELOGIND

# Uncomment to build with basu libs
# DBUS_LIBS = basu libdrm
# DBUS_FLAGS = -DWITH_BASU
